# Dracula Light for Nova

Made using [Light Dracula Generator][https://gitlab.com/dhnm/light-dracula-generator].

## Instructions

1. Download this project
2. With the Nova editor installed, double-click the downloaded file `Dracula Light.novaextension`.
3. You can choose between two themes Dracula Sunset and Dracula White in the Preferences.